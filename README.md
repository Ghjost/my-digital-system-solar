# my-digital-planets

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

## Project details

Le project My Digital Planet est un project Nuxt initié par Erwann Duclos lors d'un cour de Framework Javascript lors du MBA 1 dev full stack

## Bundles

- '@nuxtjs/axios',
- '@nuxt/content',
- '@nuxt/http',
- 'cookie-universal-nuxt',

## Some Screens

![alt text](https://i.imgur.com/bf2QQMq.jpg)
![alt text](https://i.imgur.com/bibXXZR.jpg)
![alt text](https://i.imgur.com/pM2GMmo.png)
![alt text](https://i.imgur.com/5WdW1p6.png)
![alt text](https://i.imgur.com/kR8gMJ7.png)
![alt text](https://i.imgur.com/9S5SEgf.png)
