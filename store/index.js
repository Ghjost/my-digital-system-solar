export const state = () => ({
  planets: String,
})

export const mutations = {
  savePlanets(state, data) {
    state.planets = data
  },
}

export const getters = {
  get(state) {
    return state.planets
  },
}
