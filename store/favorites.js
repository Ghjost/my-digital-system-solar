export const state = () => ({
  favorites: ['lune', 'phobos', 'europe', 'terre'],
})

export const mutations = {
  addFavorite(state, data) {
    state.favorites.push(data)
  },
  removeFavorite(state, data) {
    state.favorites.splice(state.favorites.indexOf(data), 1)
  },
}

export const getters = {
  getAll() {
    return state.favorites
  },
}
